package week1.day2;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ArraysPractise {
    private final Random rand;
    private final Scanner scanner;
    private int numsCount;
    private int[] numbers;

    public ArraysPractise(final Scanner scanner) {
        this.scanner = scanner;
        this.rand = new Random();
    }

    public void populateArray() {
        this.numsCount = getNumsCountFromConsole(scanner);
        if (this.numsCount != -1) {
            this.numbers = new int[this.numsCount];
            populateRandomNumbers(1, 1000, this.numbers);
        }
    }

    private void populateRandomNumbers(int min, int max, int[] numbers) {
        for (int i = 0; i < numbers.length; ++i) {
            numbers[i] = rand.nextInt(min, max + 1);
        }
    }

    private int getNumsCountFromConsole(final Scanner scanner) {
        int numsCount;

        showMsgOnConsole(System.out, "Please enter a number: ");
        while (true) {
            try {
                numsCount = getNumsCount(scanner);

                if (!isValidNumsCount(numsCount))
                    showMsgOnConsole(System.err, "Please enter a number be between (10000 and 500_000_000): ");
                else
                    return numsCount;

            } catch (IllegalArgumentException e) {
                showMsgOnConsole(System.err, "not valid number\n");
                break;
            }
        }
        return -1;
    }

    private int getNumsCount(final Scanner scanner) throws IllegalArgumentException {
        try {
            return scanner.nextInt();
        } catch (InputMismatchException ex) {
            Logger logger = Logger.getLogger(ArraysPractise.class.getName());
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            throw new IllegalArgumentException();
        }
    }

    private boolean isValidNumsCount(final int numsCount) {
        return numsCount > 10_000 && numsCount <= 500_000_000;
    }

    private void showMsgOnConsole(PrintStream print, String msg) {
        print.print(msg);
    }

    public long getSum() {
        assertNumbersExistence();
        long sum = 0L;
        for (int i : numbers) {
            sum += i;
        }
        return sum;

        // return Arrays.stream(numbers).sum();
        // we may use this but in so large numbers it cases overflow because it returns int
        // and the sum may be larger than int data type capacity
    }

    public long getAverage() {
        return getSum() / this.numsCount;
    }

    public List<Integer> getMostFreqNNumbers(int n) {
        assertNumbersExistence();

        Map<Integer, Integer> map = new HashMap<>();
        for (int num : numbers) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }

        Queue<Integer> heap = new PriorityQueue<>((a, b) -> map.get(b) - map.get(a));

        heap.addAll(map.keySet());

        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < n; ++i)
            res.add(heap.poll());

        return res;
    }

    public void exportToFile() throws FileNotFoundException {
        assertNumbersExistence();
        PrintWriter writer = new PrintWriter("ArrayFile");
        for (int num : numbers) {
            writer.println(num);
        }
        writer.close();
    }

    private void assertNumbersExistence() {
        if (numbers == null) {
            showMsgOnConsole(System.err, "Please populate the array first, u can use populateArray() method");
            throw new IllegalStateException();
        }
    }
}
