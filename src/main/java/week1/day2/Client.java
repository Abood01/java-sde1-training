package week1.day2;

import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {

    public static void main(String[] args) {
        ArraysPractise arraysPractise = new ArraysPractise(new Scanner(System.in));
        arraysPractise.populateArray();
        System.out.println("average: " + arraysPractise.getAverage());
        System.out.println("most three frequent  numbers");
        arraysPractise.getMostFreqNNumbers(3).forEach(System.out::println);

        System.out.println("file named ArrayFile created!");
        try {
            arraysPractise.exportToFile();
        } catch (FileNotFoundException ex) {
            Logger logger = Logger.getLogger(Client.class.getName());
            logger.log(Level.FINE, ex.getMessage(), ex);
        }


    }
}
