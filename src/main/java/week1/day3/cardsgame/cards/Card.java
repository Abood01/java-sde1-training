package week1.day3.cardsgame.cards;


import week1.day3.cardsgame.enums.CardColor;
import week1.day3.cardsgame.enums.CardType;

public abstract class Card {
    private final CardColor color;
    private final CardType type;

    public Card(final CardColor color, final CardType type) {
        this.color = color;
        this.type = type;
    }

    @Override
    public String toString() {
        return color + "  " + type + "  ";
    }
}
