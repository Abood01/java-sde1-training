package week1.day3.cardsgame.cards;


import week1.day3.cardsgame.enums.CardColor;
import week1.day3.cardsgame.enums.CardType;

public class NumberedCard extends Card {
    private final int number;

    public NumberedCard(final int number, final CardColor color, final CardType type) {
        super(color, type);
        this.number = number;
    }

    @Override
    public String toString() {
        return super.toString() + number;
    }
}
