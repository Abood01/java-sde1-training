package week1.day3.cardsgame.cards;


import week1.day3.cardsgame.enums.CardColor;
import week1.day3.cardsgame.enums.CardType;
import week1.day3.cardsgame.enums.FaceCardType;

public class FaceCard extends Card {
    private final FaceCardType faceType;

    public FaceCard(final FaceCardType cardType, final CardColor color, final CardType type) {
        super(color, type);
        this.faceType = cardType;
    }

    @Override
    public String toString() {
        return super.toString() + faceType;
    }
}
