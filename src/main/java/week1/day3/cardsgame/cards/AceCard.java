package week1.day3.cardsgame.cards;


import week1.day3.cardsgame.enums.CardColor;
import week1.day3.cardsgame.enums.CardType;

public class AceCard extends Card {
    private final String ace = "Ace";

    public AceCard(final CardColor color, final CardType type) {
        super(color, type);
    }

    @Override
    public String toString() {
        return super.toString() + ace;
    }
}
