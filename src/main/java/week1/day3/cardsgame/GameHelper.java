package week1.day3.cardsgame;


import week1.day3.cardsgame.cards.AceCard;
import week1.day3.cardsgame.cards.Card;
import week1.day3.cardsgame.cards.FaceCard;
import week1.day3.cardsgame.cards.NumberedCard;
import week1.day3.cardsgame.enums.CardColor;
import week1.day3.cardsgame.enums.CardType;
import week1.day3.cardsgame.enums.FaceCardType;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class GameHelper {
    private static final int CARDS_DECK_NUM = 52;
    private static final int PLAYERS_NUM = 4;
    private static final int SUIT_CARDS_NUM = 13;
    private static final Card[] CARDS_DECK = new Card[CARDS_DECK_NUM];
    private static final ThreadPoolExecutor POOL_EXECUTOR = (ThreadPoolExecutor) Executors.newFixedThreadPool(2);
    private static final Random RAND = new Random();

    public static Card[] getCardDeck() {
        int firstIndex = 0;
        int lastIndex = 12;

        for (int i = 0; i < PLAYERS_NUM; ++i) {
            int fFIndex = firstIndex;
            int fLIndex = lastIndex;
            int fCardDelimiter = i;

            POOL_EXECUTOR.execute(() -> populateCards(fFIndex, fLIndex, fCardDelimiter));
            firstIndex += SUIT_CARDS_NUM;
            lastIndex += SUIT_CARDS_NUM;
        }
        try {
            POOL_EXECUTOR.awaitTermination(50, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            System.err.println(e.getMessage());
        } finally {
            POOL_EXECUTOR.shutdown();
        }
        return CARDS_DECK;
    }

    private static void populateCards(int firstIndex, int lastIndex, int cardDelimiter) {
        CardType cardType = null;
        CardColor cardColor = null;
        switch (cardDelimiter) {
            case 0 -> {
                cardType = CardType.DIAMONDS;
                cardColor = CardColor.RED;
            }
            case 1 -> {
                cardType = CardType.CLUBS;
                cardColor = CardColor.BLACK;
            }
            case 2 -> {
                cardType = CardType.HEARTS;
                cardColor = CardColor.RED;
            }
            case 3 -> {
                cardType = CardType.SPADES;
                cardColor = CardColor.BLACK;
            }
        }

        CARDS_DECK[firstIndex] = new AceCard(cardColor, cardType);

        int onCardNumber = 2;
        int i;
        for (i = firstIndex + 1; i <= lastIndex - 3; ++i) {
            CARDS_DECK[i] = new NumberedCard(onCardNumber++, cardColor, cardType);
        }

        CARDS_DECK[i++] = new FaceCard(FaceCardType.KING, cardColor, cardType);
        CARDS_DECK[i++] = new FaceCard(FaceCardType.QUEEN, cardColor, cardType);
        CARDS_DECK[i] = new FaceCard(FaceCardType.JACK, cardColor, cardType);
    }

    public static void shuffleCards() {
        int len = CARDS_DECK.length;
        for (int i = 0; i < len; ++i) {
            swap(i, RAND.nextInt(len));
        }
    }

    /*              dealer
                   / / \ \
                  / /   \ \
                 / /     \ \
                / /       \ \
             (P1)(P2)    (P3)(P4)

    In my implementation The dealer distribute the cards from left to right (p1 ------> p4)
    and 1 card each time in a circular fashion "clockwise direction".
     */
    public static void distribute(final Player[] players) {
        int cardsDeckIndex = 0;
        for (int i = 0; i < SUIT_CARDS_NUM; ++i) {
            for (int j = 0; j < PLAYERS_NUM; ++j) {
                swap(j * SUIT_CARDS_NUM + i, cardsDeckIndex++);
            }
        }

        Card[] cards = new Card[SUIT_CARDS_NUM];

        cardsDeckIndex = 0;
        for (int i = 0; i < PLAYERS_NUM; ++i) {
            for (int j = 0; j < SUIT_CARDS_NUM; ++j) {
                cards[j] = CARDS_DECK[cardsDeckIndex++];
            }

            players[i] = Player.getBuilder().
                    withName(players[i].getName()).
                    withCards(cards).
                    build();
            cards = new Card[SUIT_CARDS_NUM];
        }
    }

    private static void swap(int n1, int n2) {
        Card temp = CARDS_DECK[n1];
        CARDS_DECK[n1] = CARDS_DECK[n2];
        CARDS_DECK[n2] = temp;
    }
}