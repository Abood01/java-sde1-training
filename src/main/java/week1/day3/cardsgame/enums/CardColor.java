package week1.day3.cardsgame.enums;

public enum CardColor {
    RED, BLACK;

    @Override
    public String toString() {
        return switch (this) {
            case RED -> "Red";
            case BLACK -> "Black";
        };
    }
}

