package week1.day3.cardsgame.enums;

public enum CardType {
    HEARTS, SPADES, DIAMONDS, CLUBS;

    @Override
    public String toString() {
        return switch (this) {
            case CLUBS -> "Club";
            case HEARTS -> "Heart";
            case SPADES -> "Spade";
            case DIAMONDS -> "Diamond";
        };
    }
}
