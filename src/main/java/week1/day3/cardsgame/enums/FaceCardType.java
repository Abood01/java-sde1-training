package week1.day3.cardsgame.enums;

public enum FaceCardType {
    KING, QUEEN, JACK;

    @Override
    public String toString() {
        return switch (this) {
            case JACK -> "Jack";
            case KING -> "King";
            case QUEEN -> "Queen";
        };
    }
}
