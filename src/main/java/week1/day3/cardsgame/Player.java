package week1.day3.cardsgame;


import week1.day3.cardsgame.cards.Card;

import java.util.Arrays;

public class Player {
    private final String name;
    private final int score;
    private final Card[] cards;

    public Player(String name, int score, Card[] cards) {
        this.name = name;
        this.score = score;
        this.cards = cards;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "Player Name: " + name + ",\tscore: " + score +
                "\n cards=" + Arrays.toString(cards) + "\n";
    }

    public String getName() {
        return name;
    }

    public static class Builder {
        private String name;
        private Card[] cards;

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withCards(Card[] cards) {
            this.cards = cards;
            return this;
        }

        public Player build() {
            return new Player(name, 0, cards);
        }
    }
}
