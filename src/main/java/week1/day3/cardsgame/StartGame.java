package week1.day3.cardsgame;

import java.io.PrintStream;
import java.util.Scanner;

public class StartGame {
    private final Scanner scan;
    private final PrintStream out;

    public StartGame(final Scanner scan, final PrintStream out) {
        this.scan = scan;
        this.out = out;
    }

    public void startGame() {
        String gameName = getGameName(scan);
        printLine(out);
        Player[] players = getPlayersNames(scan);

        Game newGame = new Game(gameName, players);
        newGame.start();
    }

    private String getGameName(final Scanner scanner) {
        showConsoleMsg(out, "Enter game name: ");
        return scanner.nextLine();
    }

    private Player[] getPlayersNames(final Scanner scanner) {
        Player[] players = new Player[4];
        String playerName;

        for (int i = 0; i < 4; ++i) {
            showConsoleMsg(out, "Enter player number " + (i + 1) + " name: ");
            printLine(out);

            playerName = scanner.nextLine();
            players[i] = Player.getBuilder().
                    withName(playerName).
                    build();
        }
        return players;
    }

    private void showConsoleMsg(final PrintStream stream, String msg) {
        stream.print(msg);
    }

    private void printLine(final PrintStream stream) {
        stream.println();
    }
}
