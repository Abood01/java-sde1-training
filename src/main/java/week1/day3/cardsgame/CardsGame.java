package week1.day3.cardsgame;

import java.util.Scanner;

public class CardsGame {
    public static void main(String[] args) {
        new StartGame(new Scanner(System.in), System.out).startGame();
    }
}
