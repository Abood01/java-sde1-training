package week1.day3.cardsgame;

import java.util.Arrays;

public class Game {
    private final String gameName;
    private final Player[] players;

    public Game(final String gameName, final Player[] players) {
        this.gameName = gameName;
        this.players = players;
    }

    public void start() {
        GameHelper.getCardDeck();
        GameHelper.shuffleCards();
        GameHelper.distribute(players);
        System.out.println(new String(new char[5]).replace("\0", "\r\n"));

        System.out.printf("**** Game Name: %s ****\n\n", gameName);
        Arrays.stream(players).forEach(System.out::println);
    }


}
