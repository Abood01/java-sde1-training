package week1.day1;

import java.io.PrintStream;

public class Hello {
    public static void main(String[] args) {
        String msg = "Hello from uncle Jalal";
        printOnConsole(msg, System.out);
        printOnConsole(msg, System.err);
    }

    private static void printOnConsole(final String msg, final PrintStream stream) {
        stream.println(msg);
    }
}
