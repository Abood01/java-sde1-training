package week1.day4_5;


import week1.day4_5.database.MysqlDatabase;
import week1.day4_5.database.dao.StudentDaoFactory;
import week1.day4_5.models.StudentServices;
import week1.day4_5.models.StudentsManagement;

public class Main {

    public static void main(String[] args) {
        new StudentsManagement(new StudentServices(new StudentDaoFactory(MysqlDatabase.getInstance()))).showMenu();
    }
}
