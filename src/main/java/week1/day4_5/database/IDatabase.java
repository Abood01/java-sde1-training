package week1.day4_5.database;

import java.sql.Connection;

public interface IDatabase {
    Connection getConnection();

}
