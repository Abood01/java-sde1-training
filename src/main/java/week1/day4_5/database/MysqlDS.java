package week1.day4_5.database;

import com.mysql.cj.jdbc.MysqlDataSource;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MysqlDS {
    public static MysqlDataSource getMysqlDataSource() {
        Properties properties = new Properties();
        String fileName = "/home/stranger/.config/JetBrains/IntelliJIdea2022.3/scratches/scratch.properties";

        try (FileInputStream inputStream = new FileInputStream(fileName)) {
            properties.load(inputStream);

        } catch (IOException ex) {
            Logger logger = Logger.getLogger(MysqlDS.class.getName());
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }

        MysqlDataSource mysqlDS = new MysqlDataSource();
        mysqlDS.setURL(properties.getProperty("mysql.url"));
        mysqlDS.setUser(properties.getProperty("mysql.username"));
        mysqlDS.setPassword(properties.getProperty("mysql.password"));
        return mysqlDS;
    }
}
