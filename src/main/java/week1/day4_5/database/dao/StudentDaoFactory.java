package week1.day4_5.database.dao;

import week1.day4_5.database.IDatabase;
import week1.day4_5.domainmodels.Student;


public class StudentDaoFactory implements IDaoFactory<Student> {
    private final IDatabase database;

    public StudentDaoFactory(final IDatabase database) {
        this.database = database;
    }

    @Override
    public IDao<Student> createDao() {
        return new StudentDao(database.getConnection());
    }
}
