package week1.day4_5.database.dao;

public interface IDaoFactory<T> {
    IDao<T> createDao();

}
