package week1.day4_5.database.dao;

import week1.day4_5.domainmodels.Student;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StudentDao implements IDao<Student> {
    private final Connection con;

    public StudentDao(final Connection con) {
        this.con = con;
    }

    @Override
    public void insert(Student student) {
        String query = "INSERT INTO Student(name, average, email) VALUES(?,?,?)";

        try (var pStmt = con.prepareStatement(query)) {
            pStmt.setString(1, student.name());
            pStmt.setFloat(2, student.average());
            pStmt.setString(3, student.email());
            pStmt.executeUpdate();
        } catch (SQLException ex) {
            Logger logger = Logger.getLogger(StudentDao.class.getName());
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public void update(Student student) {
        String query = "UPDATE Student SET name = ?, average = ?, email = ? WHERE ID = ?";

        try (var pStmt = con.prepareStatement(query)) {
            pStmt.setString(1, student.name());
            pStmt.setFloat(2, student.average());
            pStmt.setString(3, student.email());
            pStmt.setInt(4, student.ID());
            pStmt.executeUpdate();
        } catch (SQLException ex) {
            Logger logger = Logger.getLogger(StudentDao.class.getName());
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public void delete(int id) {
        String query = "DELETE FROM Student WHERE ID = ?";

        try (var pStmt = con.prepareStatement(query)) {
            pStmt.setInt(1, id);
            pStmt.executeUpdate();
        } catch (SQLException ex) {
            Logger logger = Logger.getLogger(StudentDao.class.getName());
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public Student getByID(int id) {
        String query = "SELECT * FROM Student WHERE ID =?";
        Student student = null;

        try (var pStmt = con.prepareStatement(query)) {
            pStmt.setInt(1, id);
            var resSet = pStmt.executeQuery();
            if (resSet.next())
                student = buildStudent(resSet.getInt("ID"),
                        resSet.getString("name"),
                        resSet.getFloat("average"),
                        resSet.getString("email"));

        } catch (SQLException ex) {
            Logger logger = Logger.getLogger(StudentDao.class.getName());
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return student;
    }

    @Override
    public Collection<Student> getAll() {
        String query = "SELECT * FROM Student";
        var students = new ArrayList<Student>();

        try (var pStmt = con.prepareStatement(query)) {
            var resSet = pStmt.executeQuery();
            while (resSet.next()) {
                Student student = buildStudent(resSet.getInt("ID"),
                        resSet.getString("name"),
                        resSet.getFloat("average"),
                        resSet.getString("email"));

                students.add(student);
            }
        } catch (SQLException ex) {
            Logger logger = Logger.getLogger(StudentDao.class.getName());
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return students;
    }

    private Student buildStudent(final int ID, final String name, final float average, final String email) {
        return Student.getBuilder().
                withID(ID).
                withName(name).
                withAverage(average).
                withEmail(email).
                build();
    }
}
