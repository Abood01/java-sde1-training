package week1.day4_5.database.dao;

import java.util.Collection;

public interface IDao<T> {
    void insert(T object);

    void update(T object);

    void delete(int id);

    T getByID(int id);

    Collection<T> getAll();

}
