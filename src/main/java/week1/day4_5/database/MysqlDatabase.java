package week1.day4_5.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MysqlDatabase implements IDatabase {
    private static final MysqlDatabase INSTANCE = new MysqlDatabase();
    private Connection con;

    private MysqlDatabase() {
        try {
            con = MysqlDS.getMysqlDataSource().getConnection();
        } catch (SQLException ex) {
            Logger logger = Logger.getLogger(MysqlDatabase.class.getName());
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public static MysqlDatabase getInstance() {
        return INSTANCE;
    }

    @Override
    public Connection getConnection() {
        return con;
    }
}
