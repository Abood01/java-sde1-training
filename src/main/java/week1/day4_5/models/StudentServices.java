package week1.day4_5.models;

import week1.day4_5.database.dao.IDao;
import week1.day4_5.database.dao.IDaoFactory;
import week1.day4_5.domainmodels.Student;

import java.util.List;

public class StudentServices {
    private final IDao<Student> studentDao;

    public StudentServices(IDaoFactory<Student> studentIDaoFactory) {
        this.studentDao = studentIDaoFactory.createDao();
    }


    public void addStudent(final Student student) {
        studentDao.insert(student);
    }

    public Student getStudByID(int id) {
        return studentDao.getByID(id);
    }

    public void updateStudent(final Student student) {
        studentDao.update(student);
    }

    public void deleteStudent(int id) {
        studentDao.delete(id);
    }

    public List<Student> getAllStudents() {
        return studentDao.getAll().stream().toList();
    }
}
