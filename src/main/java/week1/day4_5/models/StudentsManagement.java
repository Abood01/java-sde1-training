package week1.day4_5.models;

import week1.day4_5.domainmodels.Student;

import java.io.PrintStream;
import java.util.Scanner;
import java.util.regex.Pattern;

public class StudentsManagement {
    private final StudentServices studentServices;
    private final Scanner SCANNER = new Scanner(System.in);
    private final PrintStream OUT = System.out;
    private final String menuMsg;
    private final String ANSI_GREEN = "\u001B[32m";
    private final String ANSI_RESET = "\u001B[0m";
    private final String ANSI_BLUE = "\u001B[34m";
    private final String ANSI_RED = "\u001B[31m";


    // NOTE here I used the color red instead or System.err for error because it's produce strangely outputs
    // maybe system.out threads have higher priorities than system.err threads because system.out always executes before system.err
    // That my thoughts IDK why exactly

    public StudentsManagement(StudentServices studentServices) {
        this.studentServices = studentServices;
        menuMsg = """
                1- Add Student
                2- Update Student
                3- Delete Student
                4- Show All Students
                5- Quit
                =======================
                Please Enter your Choice:\s""";
    }

    public void showMenu() {
        String choice = "";
        do {
            if (!choice.equals("-1"))
                showMsgOnConsole(OUT, menuMsg);
            choice = getChoice();

            switch (choice) {
                case "-1" -> showMsgOnConsole(OUT, ANSI_RED + "please enter valid number: " + ANSI_RESET);
                case "1" -> addStud();
                case "2" -> updateStud();
                case "3" -> deleteStud();
                case "4" -> showAllStudents();
            }
        } while (!choice.equals("5"));
        showMsgOnConsole(OUT, ANSI_GREEN + "Bye Bye, See you again....");
    }

    private void addStud() {
        studentServices.addStudent(getStudentNameAndEmail());
        showMsgOnConsole(OUT, ANSI_GREEN + "Student added successfully\n" + ANSI_RESET);
    }

    private void updateStud() {
        int ID = Integer.parseInt(getStudentID());
        Student oldStudent = studentServices.getStudByID(ID);
        if (oldStudent != null) {
            showMsgOnConsole(OUT, ANSI_BLUE + oldStudent + "\n" + ANSI_RESET);
            Student updatedInfo = getStudentInfo();
            Student updatedInfoWithID = Student.getBuilder().
                    withID(ID).
                    withName(updatedInfo.name()).
                    withEmail(updatedInfo.email()).
                    withAverage(updatedInfo.average()).
                    build();

            studentServices.updateStudent(updatedInfoWithID);
            showMsgOnConsole(OUT, ANSI_GREEN + "Updated successfully\n" + ANSI_RESET);
        } else
            showMsgOnConsole(OUT, ANSI_RED + "invalid ID no student found\n" + ANSI_RESET);

    }

    private void deleteStud() {
        int ID = Integer.parseInt(getStudentID());
        Student toDeleteStudent = studentServices.getStudByID(ID);
        if (toDeleteStudent != null) {
            showMsgOnConsole(OUT, "are you sure of deleting this student?(y/yes) for deleting or anything for abort: ");
            String res = getInputFromConsole().toLowerCase();

            if (res.equals("y") || res.equals("yes")) {
                studentServices.deleteStudent(ID);
                showMsgOnConsole(OUT, ANSI_GREEN + "Student Deleted successfully\n" + ANSI_RESET);
            }
        } else
            showMsgOnConsole(OUT, ANSI_RED + "invalid ID no student found\n" + ANSI_RESET);
    }


    private void showAllStudents() {
        showMsgOnConsole(OUT, ANSI_BLUE);
        studentServices.getAllStudents().forEach(System.out::println);
        showMsgOnConsole(OUT, ANSI_RESET);
    }


    private String getStudentID() {
        showMsgOnConsole(OUT, "Enter Student's ID: ");
        String id = getIntParsableInputFromCon();

        while (id.equals("not valid")) {
            showMsgOnConsole(OUT, ANSI_RED + "please enter valid id (number): " + ANSI_RESET);
            id = getIntParsableInputFromCon();
        }
        return id;
    }

    private String getStudentName() {
        showMsgOnConsole(OUT, "Enter Student's name: ");
        String name = getInputFromConsole();

        while (!isValidName(name)) {
            showMsgOnConsole(OUT, ANSI_RED + "please enter valid student name (mandatory and without digits and at least 3 chars long): " + ANSI_RESET);
            name = getInputFromConsole();
        }
        return name;
    }

    private String getStudentEmail() {
        showMsgOnConsole(OUT, "Enter Student's email: ");
        String email = getInputFromConsole();

        while (!isValidEmail(email)) {
            showMsgOnConsole(OUT, ANSI_RED + "please enter the correct format for email (mandatory): " + ANSI_RESET);
            email = getInputFromConsole();
        }
        return email;
    }

    private String getStudentAvg() {
        showMsgOnConsole(OUT, "Enter Student's average: ");
        String avg = getFloatParsableInputFromCon();

        while (avg.equals("not valid")) {
            showMsgOnConsole(OUT, ANSI_RED + "please enter valid avg (float): " + ANSI_RESET);
            avg = getIntParsableInputFromCon();
        }
        return avg;
    }


    private Student getStudentInfo() {
        Student student = getStudentNameAndEmail();
        float avg = Float.parseFloat(getStudentAvg());

        return Student.getBuilder().
                withName(student.name()).
                withEmail(student.email()).
                withAverage(avg).
                build();
    }

    private Student getStudentNameAndEmail() {
        String name = getStudentName();
        String email = getStudentEmail();

        return Student.getBuilder().
                withName(name).
                withEmail(email).
                build();
    }


    private String getChoice() {
        String c = getInputFromConsole();
        return isValidChoice(c) ? c : "-1";
    }

    private String getInputFromConsole() {
        return SCANNER.nextLine().strip();
    }

    // parsable to int String
    private String getIntParsableInputFromCon() {
        String input = getInputFromConsole().strip();
        try {
            Integer.parseInt(input);
            return input;
        } catch (NumberFormatException e) {
            return "not valid";
        }
    }

    // parsable to float String
    private String getFloatParsableInputFromCon() {
        String input = getInputFromConsole().strip();
        try {
            Float.parseFloat(input);
            return input;
        } catch (NumberFormatException e) {
            return "not valid";
        }
    }

    private void showMsgOnConsole(final PrintStream stream, final String msg) {
        stream.print(msg);
        stream.flush();
    }

    private boolean isValidChoice(final String choice) {
        return choice.equals("1") || choice.equals("2") || choice.equals("3") || choice.equals("4") || choice.equals("5");
    }

    private boolean isValidEmail(final String email) {
        if (email.isEmpty() || email.length() > 35)
            return false;

        // this from Google simple one -- ahmad@gmail.com
        String regexPattern = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        return Pattern.compile(regexPattern).matcher(email).matches();
    }

    private boolean isValidName(final String name) {
        return !name.isEmpty() &&
                name.chars().noneMatch(Character::isDigit) &&
                name.length() >= 3 &&
                name.length() < 25;
    }
}
