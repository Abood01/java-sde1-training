package week1.day4_5.domainmodels;

public record Student(int ID, String name, float average, String email) {
    public static Builder getBuilder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "Student{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", average=" + average +
                ", email='" + email + '\'' +
                '}';
    }

    public static class Builder {
        private int ID;
        private String name;
        private float average;
        private String email;


        public Builder withID(int ID) {
            this.ID = ID;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withAverage(float average) {
            this.average = average;
            return this;
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Student build() {
            return new Student(ID, name, average, email);
        }
    }
}
